import React, { Component } from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {getCategory} from '../store/actions/getdetailcategoriesAction';

import '../assets/scss/Categories.scss'

class Categories extends Component {

  componentDidMount() {
    // console.log (this.props.getCategory())
    const category = this.props.match.params.category;
    this.props.getCategory(category)
  }

  render() {
      // console.log(this.props.categories)
      const firstNews = this.props.categories[0]
      // console.log(firstNews)
      // const newsCategory = this.props.categories.map(categoryObj => {
      //   // console.log (categoryObj)
      //   return (
      //     {categoryObj}
      //   )
      // })
      

    return (
      <div className="styling-category">
        <div className="flex flex-wrap overflow-hidden max-w-5xl mx-auto">
          <div className="w-full overflow-hidden h-full">
            <img src={firstNews && firstNews.media.secure_url} 
            alt="category-img"
            className="ml-16 my-6 w-2/3"
            />

            <h1 className="ml-16 font-serif font-semibold w-2/3 mt-4"> <Link to={`/detail/${firstNews && firstNews._id}`}>{firstNews && firstNews.title}</Link></h1>
            <h3 className="ml-16 font-semibold w-2/3 mt-6">Jhon Doe</h3>
            <h3 className="ml-16 w-2/3">{firstNews && firstNews.date.substring(0, 10)}</h3>

          </div>

          <h4 className="ml-16 w-2/3 py-3">List Lifestyle</h4>
          
          <div className="w-full overflow-hidden h-full pt-6">
          <div class="flex flex-wrap overflow-hidden">

            <div class="w-full overflow-hidden bg-green-500">

              <div class="w-1/2 overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 bg-blue-400">
                <h1 className="ml-16 font-serif font-normal text-xl w-2/3 mt-4">{firstNews && firstNews.title}</h1>
                <h3 className="ml-16 font-semibold w-2/3 mt-6">Jhon Doe</h3>
                 <h3 className="ml-16 w-2/3">{firstNews && firstNews.date.substring(0, 10)}</h3>
              </div>

              <div class="w-1/2 overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 bg-red-500">
                <img src={firstNews && firstNews.media.secure_url} 
              alt="category-img"
              className="ml-16 my-6 w-1/3"
              />

              </div>

            </div>

          </div>

              
          </div>

        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return{
    categories: state.categories1.categories
  }
}

export default connect(
  mapStateToProps,
  {getCategory}
) (withRouter(Categories));
