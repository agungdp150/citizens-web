import React, { Component } from 'react';
import HeaderS from '../components/HeaderS';
import EditProfile from '../components/EditProfile';

class EditProfilePage extends Component {
  render() {
    return (
      <div>
        <HeaderS/>
        <EditProfile/>
      </div>
    )
  }
}

export default EditProfilePage
